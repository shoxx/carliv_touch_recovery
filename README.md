This is a full touch recovery, for MTK powered phones, based on CWM.
The touch module is inspired (mostly kanged) from [Napstar full touch recovery](https://github.com/Napstar-xda/android_bootable_recovery). I had to make some changes to Napstar code, but he deserve all our gratitude for the full touch module.
To compile you need a device folder for your phone, a cm-10.1 building environment and my source. 
But:
- Make sure you put a flag on your BoardConfig.mk for screen resolution, just like in twrp:
```
DEVICE_RESOLUTION := 540x960
```

Enjoy!
